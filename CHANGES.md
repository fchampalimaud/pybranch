## v1.2.0
Simplifies async thread behavior for qt threads (no need for a separate class anymore)
Simplifies generic thread
Adds method "start_thread" to AsyncHandler to keep coherence with "start_runner"

