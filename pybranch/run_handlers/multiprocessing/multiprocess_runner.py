# !/usr/bin/python
# -*- coding: utf-8 -*-

from multiprocessing import Process, Event

from time import sleep
import logging
import datetime, sys

from pyforms import conf

from pybpodapi.com.messaging.warning import WarningMessage
from pybranch.run_handlers.generic_runner import GenericRunner
from pybranch.run_handlers.queue_handler  import QueueHandler

from pybranch.com.stdout_buffer 	import StdoutBuffer
from pybranch.com.stderr_buffer 	import StderrBuffer
from pybranch.com.messaging.stdout import StdoutMessage

logger = logging.getLogger(__name__)





class MultiprocessRunner(GenericRunner, QueueHandler, Process):
	"""

	"""

	def __init__(self, in_queue=None, out_queue=None, refresh_time=None):
		"""

		:param in_queue:
		:param out_queue:
		:param refresh_time:
		"""
		QueueHandler.__init__(self, in_queue, out_queue, refresh_time)

		self._stdout = StdoutBuffer(self)
		self._stderr = StderrBuffer(self)
		self._exit_flag = Event()

		Process.__init__(self)

	def run(self):
		"""
		Get command from in_queue, process command and output result to out_queue
		"""
		sys.stdout 			  = self._stdout
		sys.stderr 			  = self._stderr
		__builtins__['print'] = self.my_print


		self._last_exch = datetime.datetime.now()

		while not self._exit_flag.is_set():
			if self.commands_available():
				next_command = self.get_next_command()
				if next_command:
					self.process_command(next_command)
			else:
				if conf.PYBOARD_COMMUNICATION_PROCESS_TIME_2_LIVE:
					# Close the process in case there is no new commands in n milliseconds
					delta_time_in_milli = (datetime.datetime.now() - self._last_exch).total_seconds()
					if delta_time_in_milli > conf.PYBOARD_COMMUNICATION_PROCESS_TIME_2_LIVE: break
				else:
					break

				if self.refresh_time:
					sleep(self.refresh_time)

		self._exit_flag.set()


	def my_print(self, *objects, sep=' ', end='\n', file=None, flush=False):
		if file is None: file = self._stdout

		if len(objects)>1:
			msg = sep.join(map(str, objects))
		else:
			msg = objects[0]
		
		file.write(msg)

	def start_runner(self):		
		if not Process.is_alive(self):
			Process.start(self)
		else:
			self.log_msg(WarningMessage("Trying to start runner when there was already one running"), False)

	def is_running(self):
		return Process.is_alive(self)

	def stop_runner(self):
		if self.is_running(): Process.join(self)
