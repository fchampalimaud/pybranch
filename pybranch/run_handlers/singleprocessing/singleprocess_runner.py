# !/usr/bin/python
# -*- coding: utf-8 -*-

import logging
import datetime
from time import sleep

from pyforms import conf

from pybranch.run_handlers.generic_runner import GenericRunner
from pybranch.run_handlers.queue_handler import QueueHandler

logger = logging.getLogger(__name__)


class SingleprocessRunner(QueueHandler, GenericRunner):
	"""

	"""

	def __init__(self, in_queue=None, out_queue=None, refresh_time=None):
		"""

		:param in_queue:
		:param out_queue:
		:param refresh_time:
		"""

		QueueHandler.__init__(self, in_queue, out_queue, refresh_time)

	def start_runner(self):
		self._last_exch = datetime.datetime.now()

		if self.commands_available():
			next_command = self.get_next_command()
			if next_command:
				self.process_command(next_command)
		elif self.refresh_time:
			sleep(self.refresh_time)

	def is_running(self):
		return False

	def stop_runner(self):
		pass
