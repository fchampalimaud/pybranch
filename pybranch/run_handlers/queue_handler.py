# !/usr/bin/python
# -*- coding: utf-8 -*-

from pyforms import conf
from pybranch.com.messaging.stderr import StderrMessage

import logging, datetime, traceback 




logger = logging.getLogger(__name__)


class QueueHandler(object):
	"""
	Enables multiprocessing
	"""

	def __init__(self, in_queue=None, out_queue=None, refresh_time=None):
		"""

		:param in_queue:
		:param out_queue:
		:param refresh_time:
		"""

		self.refresh_time 	= refresh_time
		self.in_queue 		= in_queue  	# Messages sended by other process
		self.out_queue	 	= out_queue  	# Messages sended by this process

		# It stores a flag for each group;
		# this flag is used to validate if functions belonging to a specific groups should be executed.
		# Ex: if the first function of a group failed, we may not want to execute the next functions from the some group
		self._groups = {}

		# Saves the event to be call for the current executing function
		self._current_evt_idx = None

		self._last_exch = None  # Stores the last time where data was exchanged between processes

	def commands_available(self):
		"""
		check if the commands queue is not empty
		"""
		return not self.in_queue.empty()

	def get_next_command(self):
		"""
		return the next command to be executed from the queue. If the queue is empty return None.
		"""
		return self.in_queue.get(False) if self.commands_available() else None

	def process_command(self, command):
		self._last_exch = datetime.datetime.now()

		evt_idx, func, args, group = command

		if (group is not None) and (group not in self._groups.keys()):
			self._groups[group] = True

		# logger.info('Execute func: {0}({2}) : {1}: {3}'.format(func, self._groups.get(group, True), args, group))
		if hasattr(self, func) and self._groups.get(group, True):
			function = getattr(self, func)

			try:
				
				self._current_evt_idx 	= evt_idx
				command_output 			= function(*args)
				self._current_evt_idx 	= None
				self.log_msg(command_output, True, evt_idx)

			except Exception as err:
				logger.error(err, exc_info=True)

				# Will not execute the next functions of the same group
				if group is not None: self._groups[group] = False

				self.log_msg( StderrMessage(err), True, evt_idx)

		self._last_exch = datetime.datetime.now()


	def log_msg(self, msg, last_call, evt_idx=None):
		"""
		Queue log messages
		:param msg:
		:param last_call:
		:param evt_idx:
		"""
		if evt_idx is None:  evt_idx = self._current_evt_idx
		
		self._last_exch = datetime.datetime.now()

		self.out_queue.put( (evt_idx, msg, last_call) )
