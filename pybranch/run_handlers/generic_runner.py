# !/usr/bin/python
# -*- coding: utf-8 -*-

import logging

logger = logging.getLogger(__name__)


class GenericRunner(object):
	"""

	"""

	def __init__(self, in_queue=None, out_queue=None, refresh_time=None):
		"""

		:param in_queue:
		:param out_queue:
		:param refresh_time:
		"""

	def start_runner(self):
		pass

	def is_running(self):
		pass

	def stop_runner(self):
		pass
