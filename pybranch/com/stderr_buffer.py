import sys
from pybpodapi.com.messaging.base_message import BaseMessage
from pybranch.com.messaging.stderr import StderrMessage


class StderrBuffer(object):


	
	def __init__(self,queue):
		self.queue = queue

	def write(self, msg):
		if isinstance(msg, BaseMessage):
			self.queue.log_msg( msg, last_call=False)
		else:
			self.queue.log_msg( StderrMessage(msg), last_call=False)

	def flush(self):
		sys.__stdout__.flush()