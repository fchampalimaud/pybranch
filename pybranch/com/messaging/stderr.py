# !/usr/bin/python3
# -*- coding: utf-8 -*-

import traceback, dateutil
from pybpodapi.com.messaging.base_message import BaseMessage

class StderrMessage(BaseMessage):
	"""
	Stderr message from the server process

	.. seealso::

		:py:class:`pybpodgui_plugin.com.messaging.base_message.BoardMessage`

	"""
	MESSAGE_TYPE_ALIAS = 'stderr'
	MESSAGE_COLOR = (255,0,0)


	def __init__(self, content, host_timestamp=None):
		super(StderrMessage, self).__init__(str(content), host_timestamp)

		self.traceback = str(traceback.format_exc())

	def __str__(self):
		return "host-time:{0} pc-time:{1} {2} {3}".format(
			self.host_timestamp if self.host_timestamp is not None else '', 
			self.pc_timestamp.strftime('%Y%m%d%H%M%S') if self.pc_timestamp else '',
			self.content, self.traceback)




	def tolist(self):
		return [
			self.MESSAGE_TYPE_ALIAS, 
			str(self.pc_timestamp), 
			self.host_timestamp,
			self.content,
			self.traceback
		]

	@classmethod
	def fromlist(cls, row):
		"""
		Returns True if the typestr represents the class
		"""
		obj = cls(row[5], float(row[2]) if row[2] else None)
		obj.pc_timestamp = dateutil.parser.parse(row[1])
		obj.traceback 	 = row[4]

		return obj

