# !/usr/bin/python
# -*- coding: utf-8 -*-

__version__ = '1.4.1'

from pyforms import conf
conf += 'pybranch.settings'



import loggingbootstrap


# setup different loggers but output to single file
loggingbootstrap.create_double_logger("pybranch", conf.APP_LOG_HANDLER_CONSOLE_LEVEL,
									  conf.APP_LOG_FILENAME,
									  conf.APP_LOG_HANDLER_FILE_LEVEL)