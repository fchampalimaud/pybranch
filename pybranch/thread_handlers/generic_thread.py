# !/usr/bin/python
# -*- coding: utf-8 -*-

import logging

logger = logging.getLogger(__name__)


class GenericThread(object):
	"""

	"""

	def __init__(self, out_queue=None, in_queue=None, wait_for_results_fn=None, event_executor_fn=None):
		"""

		:param callback_function:
		:param in_queue:
		:param out_queue:
		"""

		self.wait_for_results_fn = wait_for_results_fn
		self.event_executor_fn = event_executor_fn
		self.out_queue = out_queue
		self.in_queue = in_queue

	def process_event(self, event):
		pass

	def start_thread(self):
		"""
		Simple method to abstract specific thread implementation start method
		"""
		pass
