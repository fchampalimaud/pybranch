# !/usr/bin/python
# -*- coding: utf-8 -*-

import threading

from pybranch.thread_handlers.generic_thread import GenericThread


class SimpleThread(GenericThread):
	"""
	Simple thread implementation for AsyncHandler
	"""

	def __init__(self, out_queue=None, in_queue=None, wait_for_results_fn=None, event_executor_fn=None):
		GenericThread.__init__(self, out_queue=out_queue, in_queue=in_queue, wait_for_results_fn=wait_for_results_fn,
		                       event_executor_fn=event_executor_fn)

		self.thread_instance = threading.Thread(target=self.wait_for_results_fn, args=(self.out_queue, self.in_queue))

	def process_event(self, event):
		evt_idx, result, last_call = event
		self.event_executor_fn(evt_idx, result, last_call)

	def start_thread(self):
		self.thread_instance.start()
