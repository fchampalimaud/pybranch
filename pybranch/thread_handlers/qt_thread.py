# !/usr/bin/python
# -*- coding: utf-8 -*-

from pyforms import conf

from AnyQt.QtCore import QThread
from AnyQt.QtCore import pyqtSignal

from pybranch.thread_handlers.generic_thread import GenericThread


class QtThread(GenericThread, QThread):
	"""
	Qt implementation for AsyncHandler
	"""

	update_gui = pyqtSignal(int, object, bool)

	def __init__(self, out_queue=None, in_queue=None, wait_for_results_fn=None, event_executor_fn=None,
	             mainwindow=None):
		GenericThread.__init__(
			self, out_queue=out_queue, 
			in_queue=in_queue, 
			wait_for_results_fn=wait_for_results_fn,
			event_executor_fn=event_executor_fn)

		self.mainwindow = mainwindow

	def init_qthread(self):
		"""
		Workaround to avoid "Python: RuntimeError: super-class __init__() of %S was never called" error
		"""
		QThread.__init__(self, parent=self.mainwindow)

		self.update_gui.connect(self.event_executor_fn)

	def process_event(self, event):
		evt_idx, result, last_call = event
		self.update_gui.emit(evt_idx, result, last_call)

	def run(self):
		"""
		Method invoked when thread is started
		"""
		self.wait_for_results_fn(self.out_queue, self.in_queue)

	def start_thread(self):
		"""
		Simple method to abstract thread_instance's start method
		:return:
		"""
		self.start()
