# !/usr/bin/python
# -*- coding: utf-8 -*-

import logging
from time import sleep
from pyforms import conf
from multiprocessing import Queue as MutiprocessingQueue

logger = logging.getLogger(__name__)


def dummy(e, result): pass


class AsyncHandlerEvent(object):
	"""
	AsyncPyboardEvent
	"""

	def __init__(self):
		self.last_call = None
		self.function_name = None
		self.args = []
		self.extra_args = []


class AsyncHandler(object):
	"""
	Create a thread and process runner in order to execute a job
	"""

	def __init__(self, thread_refresh_time=None, runner_refresh_time=None):

		"""

		:param thread_refresh_time:
		"""
		self.thread_refresh_time = thread_refresh_time if thread_refresh_time else conf.PYBOARD_COMMUNICATION_THREAD_REFRESH_TIME
		self.runner_refresh_time = runner_refresh_time if runner_refresh_time else conf.PYBOARD_COMMUNICATION_PROCESS_REFRESH_TIME

		self._events_list = []

		self.in_queue  = MutiprocessingQueue()  # Messages sended by other process
		self.out_queue = MutiprocessingQueue()  # Messages sended by this process

		self.runner 		= None
		self.async_thread 	= None

	#######################################################################################
	###### PROPERTIES #####################################################################
	#######################################################################################


	@property
	def runner(self):
		return self._runner

	@runner.setter
	def runner(self, value):
		self._runner = value

	@property
	def async_thread(self):
		return self._async_thread

	@async_thread.setter
	def async_thread(self, value):
		self._async_thread = value

	#######################################################################################
	###### BUILDERS #######################################################################
	#######################################################################################


	def create_runner(self):
		pass

	def create_async_thread(self):
		pass

	#######################################################################################
	###### FUNCTIONS ######################################################################
	#######################################################################################

	def wait_for_results(self, out_queue, in_queue):
		"""
		While serial process is alive, wait for results on serial
		:param out_queue:
		:param in_queue:
		"""
		while self.runner.is_running() or \
				(conf.PYBOARD_COMMUNICATION_PROCESS_TIME_2_LIVE is None and not out_queue.empty()) or \
				(len(self._events_list) > 0 and (self._events_list[-1] != None)):
			
			self.update_gui()

			if not self.in_queue.empty() and not self.runner.is_running():
				self.stop_runner()  # remove any old process
				self.start_runner()

			if not out_queue.empty():
				self.async_thread.process_event(event=out_queue.get(False))

			else:
				if self.thread_refresh_time:
					sleep(self.thread_refresh_time)

		self.stop_handler_execution()

	def update_gui(self):pass


	def event_executor(self, evt_idx, result, last_call):
		"""

		:param evt_idx:
		:param result:
		:param last_call:
		:return:
		"""
		logger.debug("event_executer parms: [[%s]] [[%s]] [[%s]]", evt_idx, result, last_call)

		logger.debug("index called %d from a list of %d elements", evt_idx, len(self._events_list))

		f, args, extra_args, function_name = self._events_list[evt_idx]
		if last_call: self._events_list[evt_idx] = None

		e = AsyncHandlerEvent()
		e.last_call 	= last_call
		e.function_name = function_name
		e.args 			= args
		e.extra_args 	= extra_args

		logger.debug("Processing data from %s: %s", function_name, str(result))

		f(e, result)

	def start_handler_execution(self):
		"""
		"""
		logger.debug("Starting handler execution")

		self.start_runner()

		self.start_thread()

	def start_thread(self):
		try:
			self.async_thread = self.create_async_thread()
			self.async_thread.start_thread()
		except Exception as e:
			self.async_thread = None
			raise e

	def start_runner(self):
		try:
			if not self.runner:
				self.runner = self.create_runner()

			self.runner.start_runner()

		except Exception as err:
			logger.error( err, exc_info=True)
			self.runner = None
			raise err

	def stop_runner(self):
		self.runner.stop_runner()
		self.runner = None

	def stop_handler_execution(self):
		"""
		Close serial connection and kill child process as soon as possible.
		"""
		self.stop_runner()

		self.async_thread = None

	def call_function(self, function_name, args=tuple(), handler_evt=dummy, extra_args=None, group=None):
		"""
		Execute command on pyboard
		:param function_name:
		:param args:
		:param handler_evt:
		:param extra_args:
		:param group:
		"""
		if not self.runner:
			self.start_handler_execution()

		logger.debug("Call function: %s", function_name)
		self._events_list.append((handler_evt, args, extra_args, function_name))
		command = (len(self._events_list) - 1, function_name, args, group)
		self.in_queue.put(command)
